import xlsxwriter


# Create an new Excel file and add a worksheet.
workbook = xlsxwriter.Workbook('images.xlsx')
worksheet = workbook.add_worksheet()

# Widen the first column to make the text clearer.
worksheet.set_column('A:A', 30)

# Insert an image.
worksheet.write('A2', 'Insert an image in a cell:')
worksheet.insert_image('B2', 'remark_background.png',{'x_scale': 9/11, 'y_scale': 1})

# Insert an image.
worksheet.write('A23', 'Insert an image in a cell:')
worksheet.insert_image('B23', 'ttd_Alfred Sugiri B.Mus,M.Mus.png',{'x_scale': 0.4, 'y_scale': 0.4})

# Insert an image.
worksheet.write('A53', 'Insert an image in a cell:')
worksheet.insert_image('B53', 'ttd_Eddie Yeh Art.Dipl.png')

# Insert an image.
worksheet.write('A73', 'Insert an image in a cell:')
worksheet.insert_image('B73', "ttd_Fennia Trilestari M.Mus.png",{'x_scale': 0.12, 'y_scale': 0.12})

# Insert an image.
worksheet.write('A93', 'Insert an image in a cell:')
worksheet.insert_image('B93', "ttd_Jin Kyung Park M.Mus.png",{'x_scale': 0.6, 'y_scale': 0.6})

# Insert an image.
worksheet.write('A113', 'Insert an image in a cell:')
worksheet.insert_image('B113', "ttd_Jun Yeong Lee.png",{'x_scale': 0.6, 'y_scale': 0.6})


# Insert an image.
worksheet.write('A133', 'Insert an image in a cell:')
worksheet.insert_image('B133', "ttd_Karolina Ott M.Mus.png",{'x_scale': 0.2, 'y_scale': 0.2})











# Insert an image with scaling.
# worksheet.write('A23', 'Insert a scaled image:')
# worksheet.insert_image('B23', 'remark_background.png', {'x_scale': 0.5, 'y_scale': 0.5})

workbook.close()