import os
import pandas as pd
import re

import xlsxwriter

jury_scale_dict = {
    "Jun Yeong Lee" : {'x_scale': 0.6, 'y_scale': 0.6},
    "Fennia Trilestari M.Mus" : {'x_scale': 0.12, 'y_scale': 0.12},
    "Alfred Sugiri B.Mus, M.Mus" : {'x_scale': 0.4, 'y_scale': 0.4},
    "Karolina Ott M.Mus" : {'x_scale': 0.2, 'y_scale': 0.2}, 
    "Eddie Yeh Art.Dipl" : {'x_scale': 1, 'y_scale': 1}, 
    "Jin Kyung Park M.Mus": {'x_scale': 0.6, 'y_scale': 0.6}
}


def get_file_list_based_on_filetype(filetype):
    return [filename for filename in os.listdir() if filetype in filename]

def get_filename_list_based_on_category(category,xlsx_file_list):
    return [filename for filename in xlsx_file_list if category in filename]

def get_filename_based_on_category_dict():
    xlsx_files_list  = get_file_list_based_on_filetype(filetype=".xlsx")
    ytp_files_list = get_filename_list_based_on_category("YTP",xlsx_files_list)
    rp_files_list = get_filename_list_based_on_category("RP",xlsx_files_list)
    v_files_list = get_filename_list_based_on_category("V",xlsx_files_list)
    xlsx_files_list.sort()
    ytp_files_list.sort()
    rp_files_list.sort()
    filename_based_on_category_dict = {
        "RP" : rp_files_list, 
        "V" : v_files_list,
        "YTP" : ytp_files_list
    }
    return filename_based_on_category_dict

def convert_row_and_column_numbers_to_cell_address(row_index,column_index):
    alphabets = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    cell_address = alphabets[column_index] + str(row_index+1)
    return cell_address

def create_juries_form():
    category_to_juries ={
        "RPLB" : ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],
        "RPB" : ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],
        "RPP" : ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],
        "RPE" : ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],
        "RPJA" : ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],
        "RPJB" : ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],
        "RPS" : ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],

        "YTPBA": ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],
        "YTPRA": ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],
        "YTPRRA": ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],
        "YTPSA": ["Jun Yeong Lee","Fennia Trilestari M.Mus","Alfred Sugiri B.Mus, M.Mus"],
        
        "VLB" : ["Karolina Ott M.Mus", "Eddie Yeh Art.Dipl", "Jin Kyung Park M.Mus"],
        "VB" : ["Karolina Ott M.Mus", "Eddie Yeh Art.Dipl", "Jin Kyung Park M.Mus"],
        "VP" : ["Karolina Ott M.Mus", "Eddie Yeh Art.Dipl", "Jin Kyung Park M.Mus"],
        "VE" : ["Karolina Ott M.Mus", "Eddie Yeh Art.Dipl", "Jin Kyung Park M.Mus"],
        "VJA" : ["Karolina Ott M.Mus", "Eddie Yeh Art.Dipl", "Jin Kyung Park M.Mus"],
        "VJB" : ["Karolina Ott M.Mus", "Eddie Yeh Art.Dipl", "Jin Kyung Park M.Mus"],
        "VS" : ["Karolina Ott M.Mus", "Eddie Yeh Art.Dipl", "Jin Kyung Park M.Mus"],
    }

    categories = list(category_to_juries.keys())

    current_dir = os.getcwd()
    filename_based_on_category_dict = get_filename_based_on_category_dict()

    # TODO: category to playlist link dict
    category_to_playlist_dic = {
        "RPLB" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUin9lMYAww11DIOB3pLlgNa",
        "RPB" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUjqwlfgvlKs_drHLhFyjFFh",
        "RPE" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUj98h3IMjy0pTLIWrY6DZle",
        "RPP" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUgE7EAIJyorbmn99s38GUw2",
        "RPJA" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUhwhuAyiAhMtROBwlJVoFe_",
        "RPJB" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUinnDjz__9kV5GHfn3eLLAO",
        "RPS" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUg9VHuX1HYVCwHQPAtVUXMF",
        "YTPBA" : "https://www.youtube.com/playlist?list=PLXkYHEc4IiUi5eDralAF32a6Aowwx2Icm",
        "YTPRA"	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUgn0w9aPDIApjOBJJjo94r2",
        "YTPRRA": "https://www.youtube.com/playlist?list=PLXkYHEc4IiUhydtjEP5kgTEMgvm-7MQz1",
        "YTPSA" : "https://www.youtube.com/playlist?list=PLXkYHEc4IiUiNGpaRtT3bqAj1IDsu0y0v",
        "VLB" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUiZ5_VAvNRqyvjXdAwcIawa",
        "VB" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUhjs8A0coVI-c0RxzJZHWYF",
        "VP" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUjzQyALxf_1fWm9Io6Kh3h1",
        "VE" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUihkToHVGgUUkXlM7l1ceLL",
        "VJA" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUgITcAI4FMd3IfD8Vmat-hw",
        "VJB" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUh5orLW0gRE5BIV8_-gDMFx",
        "VS" 	: "https://www.youtube.com/playlist?list=PLXkYHEc4IiUgeWB5glThL03ray8PCrF4W"
    }

    for category in categories:
        for jury in category_to_juries[category]:
            #import pdb
            #pdb.set_trace()
            #for filename in filename_based_on_category_dict[category]:
            print(f"Making {jury}_{category}.xlsx")
            #category = "RPB"
            #jury = "Jun Yeong Lee"
            #filename = "RPB.xlsx"
            xlsx_data = pd.read_excel(f"{category}.xlsx")
            row_size,column_size = xlsx_data.shape
            xlsx_data_list = xlsx_data.values.tolist()
            title_row = [category if i == 0 else "" for i in range(column_size)]
            jury_row = []
            for i in range(column_size):
                if i==0:
                    jury_row.append("Jury")
                elif i==1:
                    jury_row.append(jury)
                else:
                    jury_row.append("")
            empty_row = ["" for i in range(column_size)]
            playlist_row = []
            for i in range(column_size):
                if i==0:
                    playlist_row.append("Youtube Playlist")
                elif i==2:
                    playlist_row.append(category_to_playlist_dic[category])
                else:
                    playlist_row.append("")
            xlsx_data_header = xlsx_data.columns.values.tolist()
            header_rows = []
            header_rows.append(title_row)
            header_rows.append(jury_row)
            header_rows.append(empty_row)
            header_rows.append(playlist_row)
            header_rows.append(empty_row)
            header_rows.append(xlsx_data_header)
            data_to_write = header_rows + xlsx_data_list
            # Create a workbook and add a worksheet.
            workbook = xlsxwriter.Workbook(f'{jury}_{category}.xlsx')
            worksheet = workbook.add_worksheet("Playlist")
            row_index = 0
            # Iterate over the data and write it out row by row.
            for i,row in enumerate(data_to_write):
                for j,column in enumerate(row):
                    worksheet.write(i, j, column)
            worksheet = workbook.add_worksheet("Scoring and Comments")
            # RN - Full Name - Repertoire - Youtube Link - Optional(PDF Song)
            REFERENCE_NUMBER_INDEX = 0
            FULL_NAME_INDEX = 1
            REPERTOIRE_INDEX = 2
            #YOUTUBE_LINK_INDEX = 3
            #PDF_FILE_INDEX = 4
            max_column_length = 8
            row_counter = 0
            for row_index,row in enumerate(data_to_write):
                if row_index < len(header_rows):
                    continue
                jury_row = []
                for i in range(max_column_length):
                    if i == 0:
                        jury_row.append("JURY")
                    elif i == 2:
                        jury_row.append(":")
                    elif i == 3:
                        jury_row.append("=Playlist!B2")
                    else:
                        jury_row.append("")
                reference_number_row = []
                for i in range(max_column_length):
                    if i == 0:
                        reference_number_row.append("REF. NUMBER")
                    elif i == 2:
                        reference_number_row.append(":")
                    elif i == 3:
                        cell_address = convert_row_and_column_numbers_to_cell_address(row_index,REFERENCE_NUMBER_INDEX)
                        reference_number_row.append(f"=Playlist!{cell_address}")
                    else:
                        reference_number_row.append("")
                name_row = []
                for i in range(max_column_length):
                    if i == 0:
                        name_row.append("PARTICIPANT\'S FULL NAME")
                    elif i == 2:
                        name_row.append(":")
                    elif i == 3:
                        cell_address = convert_row_and_column_numbers_to_cell_address(row_index,FULL_NAME_INDEX)
                        name_row.append(f"=Playlist!{cell_address}")
                    else:
                        name_row.append("")
                repertoire_row = []
                for i in range(max_column_length):
                    if i == 0:
                        repertoire_row.append("REPERTOIRE")
                    elif i == 2:
                        repertoire_row.append(":")
                    elif i == 3:
                        cell_address = convert_row_and_column_numbers_to_cell_address(row_index,REPERTOIRE_INDEX)
                        repertoire_row.append(f"=Playlist!{cell_address}")
                    else:
                        repertoire_row.append("")
                score_row = []
                for i in range(max_column_length):
                    if i == 0:
                        score_row.append("SCORE")
                    elif i == 2:
                        score_row.append(":")
                    else:
                        score_row.append("")
                remarks_row = []
                for i in range(max_column_length):
                    if i == 0:
                        remarks_row.append("REMARKS")
                    elif i == 2:
                        remarks_row.append(":")
                    else:
                        remarks_row.append("")
                to_write_list = [jury_row,reference_number_row,name_row,repertoire_row,score_row,remarks_row]
                background_height = 13
                jury_height = 4
                empty_row = ["" for i in range(max_column_length)]
                for i in range(background_height + jury_height):
                    to_write_list.append(empty_row)
                jury_signature_name_row = []
                for i in range(max_column_length):
                    if i == 4:
                        jury_signature_name_row.append("=Playlist!B2")
                    else:
                        jury_signature_name_row.append("")  
                to_write_list.append(jury_signature_name_row)
                # Add space for next participant
                for i in range(4):
                    to_write_list.append(empty_row)
                #######################################################
                ## Writing Process
                #######################################################
                # Write to Spreadsheet
                for now_i,row_now in enumerate(to_write_list):
                    for now_j,column_now in enumerate(row_now):
                        worksheet.write(now_i+row_counter, now_j, column_now)
                # Insert Remark Background
                remark_background_address = convert_row_and_column_numbers_to_cell_address(row_index=row_counter+5,column_index=3)
                worksheet.insert_image(remark_background_address, 'remark_background.png',{'x_scale': 9/11, 'y_scale': 1})
                # Insert Signature
                signature_address = convert_row_and_column_numbers_to_cell_address(row_index=row_counter+19,column_index=5)
                worksheet.insert_image(signature_address, f'ttd_{jury}.png',jury_scale_dict[jury])
                row_counter += len(to_write_list)
            workbook.close()




    # TODO: DO these
    # for each jury:
    #   for each category:
    #      paste category,ref_number,name,repertoire
    #      add score and remarks
    #      insert image for remarks
    #      insert signature
    #      insert jury name

if __name__=="__main__":
    create_juries_form()