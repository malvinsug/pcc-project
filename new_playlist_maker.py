import pandas as pd
from youtube import YoutubeClient
from googleapiclient.errors import HttpError


NAME_INDEX = 1
LINK_INDEX = 3
CATEGORY_TO_PLAYLIST_LINK  = {
    "RPLB" 	    : "PLXkYHEc4IiUin9lMYAww11DIOB3pLlgNa",
    "RPB" 	    : "PLXkYHEc4IiUjqwlfgvlKs_drHLhFyjFFh",
    "RPE" 	    : "PLXkYHEc4IiUj98h3IMjy0pTLIWrY6DZle",
    "RPP" 	    : "PLXkYHEc4IiUgE7EAIJyorbmn99s38GUw2",
    "RPJA" 	    : "PLXkYHEc4IiUhwhuAyiAhMtROBwlJVoFe_",
    "RPJB" 	    : "PLXkYHEc4IiUinnDjz__9kV5GHfn3eLLAO",
    "RPS" 	    : "PLXkYHEc4IiUg9VHuX1HYVCwHQPAtVUXMF",
    "YTPBA" 	: "PLXkYHEc4IiUi5eDralAF32a6Aowwx2Icm",
    "YTPRA" 	: "PLXkYHEc4IiUgn0w9aPDIApjOBJJjo94r2",
    "YTPRRA"	: "PLXkYHEc4IiUhydtjEP5kgTEMgvm-7MQz1",
    "YTPSA" 	: "PLXkYHEc4IiUiNGpaRtT3bqAj1IDsu0y0v",
    "VLB" 	    : "PLXkYHEc4IiUiZ5_VAvNRqyvjXdAwcIawa",
    "VB" 	    : "PLXkYHEc4IiUhjs8A0coVI-c0RxzJZHWYF",
    "VP" 	    : "PLXkYHEc4IiUjzQyALxf_1fWm9Io6Kh3h1",
    "VE" 	    : "PLXkYHEc4IiUihkToHVGgUUkXlM7l1ceLL",
    "VJA" 	    : "PLXkYHEc4IiUgITcAI4FMd3IfD8Vmat-hw",
    "VJB" 	    : "PLXkYHEc4IiUh5orLW0gRE5BIV8_-gDMFx",
    "VS" 	    : "PLXkYHEc4IiUgeWB5glThL03ray8PCrF4W"
}

def insert_video_to_playlist(playlist_id, row, video_id, youtube):
    try:
        print(f"Inserting {row[NAME_INDEX]}'s video...")
        YoutubeClient.add_video_to_playlist(youtube, video_id, playlist_id)
        print(f"Inserting is successful!\n")
    except HttpError as exception:
        print(exception)
        print(f"{row[NAME_INDEX]} can't be inserted!\n")
        video_id = "fZrm9h3JRGs"  # LANG LANG VIDEO
        try:
            YoutubeClient.add_video_to_playlist(youtube,video_id,playlist_id)
        except HttpError as exception:
            print(exception)
            print(f"You don't have a quota anymore for today..")
            exit()


def extract_video_id(video_link):
    if "https://youtu.be/" in video_link:
        video_id = video_link.replace("https://youtu.be/", "")
    elif "https://www.youtube.com/watch?v=" in video_link:
        video_id = video_link.replace("https://www.youtube.com/watch?v=", "")
    elif "https://youtube.com/channel/" in video_link:
        video_id = video_link.replace("https://youtube.com/channel/", "")
    elif "https://youtube.com/shorts/" in video_link and "?feature=share" in video_link:
        video_id = video_link.replace("https://youtube.com/shorts/","").replace("?feature=share","")
    else:
        print("WARNING!: The given link does not fit the regex.")
        return video_link
    print(f"{video_link} --> {video_id}")
    return video_id

def create_youtube_playlist(category,values):
    print("Preparing youtube service ...")
    youtube = YoutubeClient.create_service()
    for row in values:
        playlist_id = CATEGORY_TO_PLAYLIST_LINK[category]
        video_link = row[LINK_INDEX]
        video_id = extract_video_id(video_link)
        insert_video_to_playlist(playlist_id,row,video_id,youtube)

def create_playlist():
    category = input("Please choose the category : ")
    filename = category+".xlsx"
    participant_data_list = pd.read_excel(filename).values.tolist()
    print(f"Row : {len(participant_data_list)}")
    print(f"Category : {category}")
    create_youtube_playlist(category,participant_data_list)

if __name__=="__main__":
    create_playlist()



