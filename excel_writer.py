# INFO : This script prepares file for jury comment.
# Probably we don't use this anymore

from pysheet_client import PysheetClient
import pickle
from resource_manager import Resources
import pandas as pd

def create_excel_files():
    group_list = [
        "fun_piano",
        "regular piano",
        "violin",
        "vocal",
        "young artist piano"
    ]

    with open('raw_data.pickle', 'rb') as f:
        raw_data = pickle.load(f)

    headers = [
        "tanggal",
        "email",
        "nama",
        "group",
        "regular piano",
        "fun_piano",
        "violin",
        "vocal",
        "young artist piano",
        "repetoar",
        "youtube_link",
    ]

    dataframe = pd.DataFrame(raw_data, columns=headers)

    filtered_headers = [
                "tanggal",
                "email",
                "nama",
                "repetoar",
                "youtube_link",
            ]

    group_index = 0
    for index,category in enumerate(Resources.CATEGORY_LIST):
        pysheet_client = PysheetClient(f"Database_{category}.xlsx")
        pysheet_client.access_spreadsheet(f"{category}")

        if index == 3 or index == 9 or index == 13 or index == 16:
            group_index += 1

        filtered_data = dataframe[dataframe[group_list[group_index]]== category]
        filtered_data = filtered_data[filtered_headers]
        print(filtered_data)
        print(filtered_data.shape)
        print("")
        filtered_data_list = filtered_data.values.tolist()
        pysheet_client.write_report(filtered_headers)
        for row in filtered_data_list:
            pysheet_client.write_report(row)
        pysheet_client.save()




if __name__=="__main__":
    create_excel_files()
    # pysheet_client = PysheetClient(f"Database_.xlsx")
    # pysheet_client.access_spreadsheet("DATABASE_MASTER")
#
    # with open('raw_data.pickle', 'rb') as f:
    #     raw_data = pickle.load(f)
#
#
    # headers = [
    #             "tanggal",
    #             "email",
    #             "nama",
    #             "group",
    #             "regular piano",
    #             "fun_piano",
    #             "violin",
    #             "vocal",
    #             "young artist piano",
    #             "repetoar",
    #             "youtube_link",
    #         ]
#
    # pysheet_client.write_report(headers)
#
#
    # for row in raw_data:
    #     pysheet_client.write_report(row)
#
    # pysheet_client.save()
#

