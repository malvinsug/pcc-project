from google_sheet import GoogleSheetClient
from youtube import YoutubeClient
from resource_manager import Resources
from googleapiclient.errors import HttpError


class PlaylistMaker:
    NAME_INDEX = 1
    LINK_INDEX = 3
    # DATE_INDEX = 0
    # EMAIL_INDEX = 1
    # NAME_INDEX = 2
    # GROUP_INDEX = 3
    # RP_INDEX = 4
    # FP_INDEX = 5
    # VIOLIN_INDEX = 6
    # VOCAL_INDEX = 7
    # YAP_INDEX = 8
    # REPERTOIRE_INDEX = 9
    # LINK_INDEX = 10

    # GROUP_DICT = {
    #     "REGULAR PIANO": RP_INDEX,
    #     "FUN PIANO": FP_INDEX,
    #     "VIOLIN": VIOLIN_INDEX,
    #     "VOCAL": VOCAL_INDEX,
    #     "YOUNG ARTIST PIANO": YAP_INDEX,
    # }
    CATEGORY_TO_PLAYLIST_LINK  = {
        "RPLB" 	    : "PLXkYHEc4IiUin9lMYAww11DIOB3pLlgNa",
        "RPB" 	    : "PLXkYHEc4IiUjqwlfgvlKs_drHLhFyjFFh",
        "RPE" 	    : "PLXkYHEc4IiUj98h3IMjy0pTLIWrY6DZle",
        "RPP" 	    : "PLXkYHEc4IiUgE7EAIJyorbmn99s38GUw2",
        "RPJA" 	    : "PLXkYHEc4IiUhwhuAyiAhMtROBwlJVoFe_",
        "RPJB" 	    : "PLXkYHEc4IiUinnDjz__9kV5GHfn3eLLAO",
        "RPS" 	    : "PLXkYHEc4IiUg9VHuX1HYVCwHQPAtVUXMF",
        "YTPBA" 	: "PLXkYHEc4IiUi5eDralAF32a6Aowwx2Icm",
        "YTPRA" 	: "PLXkYHEc4IiUgn0w9aPDIApjOBJJjo94r2",
        "YTPRRA"	: "PLXkYHEc4IiUhydtjEP5kgTEMgvm-7MQz1",
        "YTPSA" 	: "PLXkYHEc4IiUiNGpaRtT3bqAj1IDsu0y0v",
        "VLB" 	    : "PLXkYHEc4IiUiZ5_VAvNRqyvjXdAwcIawa",
        "VB" 	    : "PLXkYHEc4IiUhjs8A0coVI-c0RxzJZHWYF",
        "VP" 	    : "PLXkYHEc4IiUjzQyALxf_1fWm9Io6Kh3h1",
        "VE" 	    : "PLXkYHEc4IiUihkToHVGgUUkXlM7l1ceLL",
        "VJA" 	    : "PLXkYHEc4IiUgITcAI4FMd3IfD8Vmat-hw",
        "VJB" 	    : "PLXkYHEc4IiUh5orLW0gRE5BIV8_-gDMFx",
        "VS" 	    : "PLXkYHEc4IiUgeWB5glThL03ray8PCrF4W"
    }

    def __init__(self, process_logger):
        self.process_logger = process_logger

    def create_youtube_playlist(self, values):
        print("Preparing youtube service ...")
        youtube = YoutubeClient.create_service()
        for row in values:
            playlist_id = self.get_playlist_id(row)
            video_link = row[PlaylistMaker.LINK_INDEX]
            video_id = self.extract_video_id(video_link)
            self.insert_video_to_playlist(playlist_id,row,video_id,youtube)

    def insert_video_to_playlist(self, playlist_id, row, video_id, youtube):
        try:
            print(f"Inserting {row[PlaylistMaker.NAME_INDEX]}'s video...")
            YoutubeClient.add_video_to_playlist(youtube, video_id, playlist_id)
            print(f"Inserting is successful!\n")
            self.process_logger.save_valid_data(row)
        except HttpError as exception:
            print(exception)
            print(f"{row[PlaylistMaker.NAME_INDEX]} can't be inserted!\n")
            video_id = "fZrm9h3JRGs"  # LANG LANG VIDEO
            try:
                YoutubeClient.add_video_to_playlist(youtube,video_id,playlist_id)
                self.process_logger.save_invalid_data(row)
            except HttpError as exception:
                print(exception)
                print(f"You don't have a quota anymore for today..")
                self.process_logger.create_report()
                exit()


    def extract_video_id(self, video_link):
        if "https://youtu.be/" in video_link:
            video_id = video_link.replace("https://youtu.be/", "")
        elif "https://www.youtube.com/watch?v=" in video_link:
            video_id = video_link.replace("https://www.youtube.com/watch?v=", "")
        elif "https://youtube.com/channel/" in video_link:
            video_id = video_link.replace("https://youtube.com/channel/", "")
        elif "https://youtube.com/shorts/" in video_link and "?feature=share" in video_link:
            video_id = video_link.replace("https://youtube.com/shorts/","").replace("?feature=share","")
        else:
            print("WARNING!: The given link does not fit the regex.")
            return video_link
        print(f"{video_link} --> {video_id}")
        return video_id

    def get_playlist_id(self, row):
        category = None
        for key in 
        #category_index = PlaylistMaker.GROUP_DICT[row[PlaylistMaker.GROUP_INDEX]]
        #category = row[category_index]
        playlist_id = Resources.PLAYLIST_IDS_DICT[category]
        print(playlist_id)
        return playlist_id

    def get_table_from_spreadsheet(
        self, spreadsheet_range, spreadsheet_id, spreadsheet_name
    ):
        print("Create Service using OAuthToken...")
        google_sheet = GoogleSheetClient.create_service()
        print(
            f"Finding {spreadsheet_range} from {spreadsheet_name} = {spreadsheet_id}..."
        )
        values = GoogleSheetClient.get_block(
            google_sheet, spreadsheet_id, spreadsheet_range
        )
        print("The request is successful!")

        self.process_logger.total_row = len(values)
        print(f"Total Row: {self.process_logger.total_row}")
        return values

    def prepare_input(self):
        spreadsheet_name = input(
            "Welcome to Youtube Playlist Maker! Enter your spreadsheet file name:"
        )
        # spreadsheet_id = "1dOFdAHZXz0j3k0N_Karu0iQGUmWEDlrwLKp5qoJEbvk"
        spreadsheet_id = Resources.EXCEL_FILE_DICT[spreadsheet_name]
        # spreadsheet_range = "Sheet1!A2:K23"
        spreadsheet_range = input(
            "Please give us the range. Example: Form Responses 1!A2:K48"
        )
        self.process_logger.add("filename", spreadsheet_name)
        self.process_logger.add("range", spreadsheet_range)

        return spreadsheet_range, spreadsheet_id, spreadsheet_name
