# INFO : Setting google credentials for accessing spreadsheet.
# This is important! We need this to read the files

from Google import Create_Service


class GoogleSheetClient:
    CLIENT_SECRET_FILE = "client_secret_2.json"
    API_NAME = "sheets"
    API_VERSION = "v4"
    SCOPES = ["https://www.googleapis.com/auth/spreadsheets"]

    @staticmethod
    def create_service():
        return Create_Service(
            GoogleSheetClient.CLIENT_SECRET_FILE,
            GoogleSheetClient.API_NAME,
            GoogleSheetClient.API_VERSION,
            GoogleSheetClient.SCOPES,
        )

    @staticmethod
    def get_block(google_sheet, spreadsheet_id, range):
        response = (
            google_sheet.spreadsheets()
            .values()
            .get(spreadsheetId=spreadsheet_id, range=range)
            .execute()
        )

        return response["values"]


if __name__ == "__main__":
    pass
    # TEST: read certain block in excel file
    spreadsheet_id = (
        "1UqZNejQkf92bFk35snOQeYhLnRfSS0ANzFlbRO8bemw"  # masih salinan bos!
    )
    google_sheet = GoogleSheetClient.create_service()
    range = "Form Responses 1!"
    values = GoogleSheetClient.get_block(google_sheet, spreadsheet_id, range)
    import pandas as pd

    values = pd.DataFrame(values)
    print(values)
