# INFO : Pickling for certain data from spreadsheet file (taken from Google Sheet)
# Maybe useful for debugging

from resource_manager import Resources
from google_sheet import GoogleSheetClient
import pickle

def get_table_from_spreadsheet(
        spreadsheet_range, spreadsheet_id, spreadsheet_name
):
    print("Create Service using OAuthToken...")
    google_sheet = GoogleSheetClient.create_service()
    print(
        f"Finding {spreadsheet_range} from {spreadsheet_name} = {spreadsheet_id}..."
    )
    values = GoogleSheetClient.get_block(
        google_sheet, spreadsheet_id, spreadsheet_range
    )
    print("The request is successful!")

    return values


if __name__=="__main__":
    spreadsheet_name = input(
        "Welcome to Youtube Playlist Maker! Enter your spreadsheet file name:"
    )
    # spreadsheet_id = "1dOFdAHZXz0j3k0N_Karu0iQGUmWEDlrwLKp5qoJEbvk"
    spreadsheet_id = Resources.EXCEL_FILE_DICT[spreadsheet_name]
    # spreadsheet_range = "Sheet1!A2:K23"
    spreadsheet_range = input(
        "Please give us the range. Example: Form Responses 1!A2:K48"
    )

    values = get_table_from_spreadsheet(spreadsheet_range,spreadsheet_id,spreadsheet_name)

    with open("raw_data_form.pickle", "wb") as f:
        pickle.dump(values, f)

    print("Pickling is successful!")