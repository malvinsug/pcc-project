import yaml
import os


class Resources:
    with open(str(os.getenv("PLAYLIST_DICT_FILE"))) as file:
        PLAYLIST_IDS_DICT = yaml.load(file, Loader=yaml.FullLoader)

    CATEGORY_LIST = list(PLAYLIST_IDS_DICT.keys())

    with open(str(os.getenv("EXCEL_FILE_DICT"))) as file:
        EXCEL_FILE_DICT = yaml.load(file, Loader=yaml.FullLoader)

    with open(str(os.getenv("JURY_LIST"))) as file:
        JURY_DICT = yaml.load(file, Loader=yaml.FullLoader)

    JURY_NAME = dict(list(JURY_DICT.items())[0:12 // 2])
    JURY_INIT = dict(list(JURY_DICT.items())[12 // 2:])



if __name__ == "__main__":
    pass
    # TEST: check if the needed resources are ready
    # print(Resources.PLAYLIST_IDS_DICT)
    # print(Resources.CATEGORY)
    # print(Resources.PLAYLIST_IDS_DICT[Resources.CATEGORY[3]])
    # TEST: check validity of all playlist link
    # with open("playlist_dic.yml") as file:
    #    playlistIDs = yaml.load(file, Loader=yaml.FullLoader)
    #
    # youtube = YoutubeClient.create_service()
    # for playlistID in playlistIDs:
    #    response = youtube.playlistItems().list(
    #                part='contentDetails',
    #                playlistId=playlistIDs[playlistID],
    #                maxResults=50
    #                ).execute()
    #    print(response)
