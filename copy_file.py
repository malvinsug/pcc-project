# INFO: This script copies a template for Jury Comment File. 
# I don't think we need this anymore.

import os
import shutil
from resource_manager import Resources

def generate(filename):
    src_dir = os.getcwd() #get the current working dir
    print(src_dir)

    if "Violin" in filename:
        subfolder = "Violin"
    elif "Vocal" in filename:
        subfolder = "Vocal"
    elif "Piano" in filename:
        subfolder = "Piano"
    os.listdir()

    dest_dir = src_dir+"/"+subfolder
    src_file = "/Users/malvinsugiri/Documents/pcc-project/CATEGORY_SUBCATEGORY_MASTER.xlsx"
    shutil.copy(src_file,dest_dir) #copy the file to destination dir

    dst_file = os.path.join(dest_dir,'CATEGORY_SUBCATEGORY_MASTER.xlsx')
    new_dst_file_name = os.path.join(dest_dir, filename)

    os.rename(dst_file, new_dst_file_name)#rename

if __name__== "__main__":
    # print(Resources.JURY_INIT)
    print(Resources.CATEGORY_LIST)
    for category in Resources.CATEGORY_LIST:
        category = category.replace(" ", "_")
        if "Violin" in category:
            for i in range(1,3):
                filename = f"{category}_{Resources.JURY_INIT[f'Ini_Violin_{i}']}.xlsx"
                generate(filename)
        elif "Vocal" in category:
            for i in range(1,3):
                filename = f"{category}_{Resources.JURY_INIT[f'Ini_Vocal_{i}']}.xlsx"
                generate(filename)
        elif "Piano" in category:
            for i in range(1,3):
                filename = f"{category}_{Resources.JURY_INIT[f'Ini_Piano_{i}']}.xlsx"
                generate(filename)
        else:
            print("You're messed up dude!")
            exit()