# # Create an new Excel file and add a worksheet.
# workbook = xlsxwriter.Workbook('images.xlsx')
# worksheet = workbook.add_worksheet()
#
# # Widen the first column to make the text clearer.
# worksheet.set_column('A:A', 30)
#
# # Insert an image.
# worksheet.write('A2', 'Insert an image in a cell:')
# size = {
#     'x_scale': 0.6,
#     'y_scale': 0.6
# }
# worksheet.insert_image('B2', 'back_com.png',size)

# Example
# text = 'A textbox with changed dimensions'
# options = {
#     'width': 761.333 * 0.6,
#     'height': 437.333 * 0.6,
#     'fill': {'none': True}
# }
# worksheet.insert_textbox(1, 1, text, options)

# workbook.close()