from pysheet_client import PysheetClient
from openpyxl.drawing.image import Image
import os

import xlsxwriter

if __name__=="__main__":
    instruments = ["Piano","Violin","Vocal"]
    for instrument in instruments:
        os.chdir(instrument)
        files = os.listdir()
        files.sort()
        for file in files:
            print(file)
            if file == ".DS_Store":
                continue
            workbook = xlsxwriter.Workbook(file)
            spreadsheet = workbook.get_worksheet_by_name("JURI 1")
            if spreadsheet == None:
                spreadsheet = workbook.get_worksheet_by_name("JURI 2")
                print(spreadsheet)
            row_index = 36
            for i in range(59):
                # writer.put_image("../back_com.png",f"C{row_index}")
                # writer.workbook.add_textbox(row_index=row_index, col_index= 3)
                ttxt = "Write your comment here"
                img = Image("../back_com.png")
                width = img.width * 0.5
                height = img.height * 0.5
                xlwropts = {'width': width, 'height': height}
                spreadsheet.insert_textbox(row_index, 3, ttxt, xlwropts)
                row_index += 29
                workbook.close()
                exit()
            # writer.save()
        os.chdir("../")