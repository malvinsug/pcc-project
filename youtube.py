from Google import Create_Service


class YoutubeClient:
    CLIENT_SECRET_FILE = "client_secret_2022_malvin.json"#"client_secret_2022.json" #"client_secret.json"
    API_NAME = "youtube"
    API_VERSION = "v3"
    SCOPES = ["https://www.googleapis.com/auth/youtube"]

    @staticmethod
    def create_service():
        return Create_Service(
            YoutubeClient.CLIENT_SECRET_FILE,
            YoutubeClient.API_NAME,
            YoutubeClient.API_VERSION,
            YoutubeClient.SCOPES,
        )

    @staticmethod
    def add_video_to_playlist(youtube, videoID, playlistID):
        add_video_request = (
            youtube.playlistItems()
            .insert(
                part="snippet",
                body={
                    "snippet": {
                        "playlistId": playlistID,
                        "resourceId": {"kind": "youtube#video", "videoId": videoID}
                        # 'position': 0
                    }
                },
            )
            .execute()
        )

        return add_video_request


if __name__ == "__main__":
    pass
    # TEST: put link on playlist
    # playlistID = "PLXkYHEc4IiUgauTx6CbuBVhgRHZ2H3Yf5"
    # videoID = "-TXEqEhL_4U"
    # youtube = YoutubeClient.create_service()
    # response = YoutubeClient.add_video_to_playlist(youtube, videoID, playlistID)
    # print(response)
