# INFO: A Background maker for Jury's Comment. This would probably not used anymore,
# we want to change this by generating a pdf file for jury comment.

from pysheet_client import PysheetClient
import os

if __name__=="__main__":
    instruments = ["Piano","Violin","Vocal"]
    for instrument in instruments:
        os.chdir(instrument)
        files = os.listdir()
        files.sort()
        for file in files:
            if file == "Fun_Piano_BEGINNER_AA.xlsx" or file == "Fun_Piano_BEGINNER_DVP.xlsx"or file == "Fun_Piano_ELEMENTARY_AA.xlsx" or file == "Fun_Piano_ELEMENTARY_DVP.xlsx" or file == ".DS_Store":
                continue
            writer = PysheetClient(file)
            writer.access_spreadsheet("JURI 1")
            row_index = 36
            for i in range(59):
                writer.put_image("../back_com.png",f"C{row_index}")
                row_index += 29
            writer.save()
        os.chdir("../")