from pysheet_client import PysheetClient


class ProcessLogger:
    def __init__(self, datetime):
        self.data = {"datetime": datetime}
        self.total_row = 0
        self.invalid_data = []
        self.valid_data = []

    def add(self, key, value):
        self.data[key] = value

    def save_invalid_data(self, row):
        self.invalid_data.append(row)

    def save_valid_data(self, row):
        self.valid_data.append(row)

    def create_report(self):
        pysheet_client = PysheetClient(f"Laporan_YoutubeLink.xlsx")

        pysheet_client.access_spreadsheet("laporan_general")
        pysheet_client.write_report(
            ["tanggal", "nama_file", "range", "total", "data_tidak_valid", "data_valid"]
        )
        row = [
            self.data["datetime"],
            self.data["filename"],
            self.data["range"],
            self.total_row,
            len(self.invalid_data),
            len(self.valid_data),
        ]
        pysheet_client.write_report(row)

        pysheet_client.access_spreadsheet("data_tidak_valid")
        pysheet_client.write_report(
            [
                "tanggal",
                "email",
                "nama",
                "group",
                "regular piano",
                "fun_piano",
                "violin",
                "vocal",
                "young artist piano",
                "repetoar",
                "youtube_link",
            ]
        )
        for row in self.invalid_data:
            pysheet_client.write_report(row)

        pysheet_client.access_spreadsheet("data_valid")
        pysheet_client.write_report(
            [
                "tanggal",
                "email",
                "nama",
                "group",
                "regular piano",
                "fun_piano",
                "violin",
                "vocal",
                "young artist piano",
                "repetoar",
                "youtube_link",
            ]
        )
        for row in self.valid_data:
            pysheet_client.write_report(row)



        pysheet_client.save()
