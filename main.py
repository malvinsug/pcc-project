# INFO : This script is the main script. It creates file 
import datetime
from process_logger import ProcessLogger
from playlist_maker import PlaylistMaker


def main():
    now = datetime.datetime.now()
    process_logger = ProcessLogger(now)
    playlist_maker = PlaylistMaker(process_logger)
    (
        spreadsheet_range,
        spreadsheet_id,
        spreadsheet_name,
    ) = playlist_maker.prepare_input()
    values = playlist_maker.get_table_from_spreadsheet(
        spreadsheet_range, spreadsheet_id, spreadsheet_name
    )
    playlist_maker.create_youtube_playlist(values)
    process_logger.create_report()


if __name__ == "__main__":
    main()
