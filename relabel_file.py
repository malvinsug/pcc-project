from pysheet_client import PysheetClient
import os
from resource_manager import Resources

if __name__ == "__main__":
    youtube_link = "https://www.youtube.com/playlist?list="
    youtube_link_row = 4
    youtube_link_column = 4

    category_row_1 = 2
    category_column_1 = 2

    category_row_2 = 69
    category_column_2 = 2

    jury_row_1 = 72
    jury_column_1 = 3

    jury_row_2 = 73
    jury_column_2 = 3

    instrument_list = ["Violin","Vocal","Piano"]
    source = os.getcwd()
    for instrument in instrument_list:
        os.chdir(source+"/"+instrument)
        file_list = os.listdir()
        file_list.sort()
        if ".DS_Store" in file_list:
            file_list.remove(".DS_Store")

        for file in file_list:
            client = PysheetClient(file)
            client.access_spreadsheet("PLAYLIST")
            file = file.replace("_"," ")
            length = len(file)
            if "DVP" in file:
                category = file[:length - 9]
            else:
                category = file[:length - 8]
            client.overwrite(value=category,row_index=category_row_1,column_index=category_column_1)
            client.overwrite(value=category,row_index=category_row_2,column_index=category_column_2)

            youtube_id = Resources.PLAYLIST_IDS_DICT[category]
            full_link = youtube_link + youtube_id
            client.overwrite(value=full_link, row_index=youtube_link_row, column_index=youtube_link_column)

            if instrument == "Violin":
                jury_name_1 = Resources.JURY_NAME["Violin_1"]
                client.overwrite(value=jury_name_1, row_index=jury_row_1, column_index=jury_column_1)
                jury_name_2 = Resources.JURY_NAME["Violin_2"]
                client.overwrite(value=jury_name_2, row_index=jury_row_2, column_index=jury_column_2)
            elif instrument == "Vocal":
                jury_name_1 = Resources.JURY_NAME["Vocal_1"]
                client.overwrite(value=jury_name_1, row_index=jury_row_1, column_index=jury_column_1)
                jury_name_2 = Resources.JURY_NAME["Vocal_2"]
                client.overwrite(value=jury_name_2, row_index=jury_row_2, column_index=jury_column_2)
            elif instrument == "Piano":
                jury_name_1 = Resources.JURY_NAME["Piano_1"]
                client.overwrite(value=jury_name_1, row_index=jury_row_1, column_index=jury_column_1)
                jury_name_2 = Resources.JURY_NAME["Piano_2"]
                client.overwrite(value=jury_name_2, row_index=jury_row_2, column_index=jury_column_2)

            client.save()




