import openpyxl
from openpyxl.drawing.image import Image
from copy import copy


class PysheetClient:
    def __init__(self, filename):
        try:
            workbook = openpyxl.load_workbook(filename)
        except FileNotFoundError:
            workbook = openpyxl.Workbook()

        self.workbook = workbook
        self.spreadsheet = None
        self.name = filename

    def access_spreadsheet(self, sheet_name):
        try:
            self.workbook.active = self.workbook[sheet_name]
            self.spreadsheet = self.workbook.get_active_sheet()
        except KeyError:
            self.workbook.create_sheet(sheet_name)
            self.workbook.active = self.workbook[sheet_name]
            self.spreadsheet = self.workbook.get_active_sheet()

    def create_column_name(self, column_list):
        for j in range(len(column_list)):
            cell = self.spreadsheet.cell(1, j + 1)
            cell.value = column_list[j]

    def column_name_exist(self):
        return self.spreadsheet.cell(1, 1).value is not None

    def find_empty_row(self):
        row_index = 1
        cell = self.spreadsheet.cell(row_index, 1)
        while cell.value is not None:
            row_index += 1
            cell = self.spreadsheet.cell(row_index, 1)

        return row_index

    def write_report(self, report_list):
        row_index = self.find_empty_row()
        for report_index in range(len(report_list)):
            cell = self.spreadsheet.cell(row_index, report_index + 1)
            cell.value = report_list[report_index]

    def save(self):
        self.workbook.save(self.name)

    def get_row_cell(self, row_number):
        return list(self.spreadsheet.rows)[row_number]

    def get_all_row_cell_size(self):
        return len(self.get_all_row_cell())

    def get_all_row_cell(self):
        return list(self.spreadsheet.rows)

    def get_worksheet_names(self):
        return self.workbook.sheetnames

    def put_image(self,image_path,position):
        img = Image(image_path)
        img.width = img.width * 0.5
        img.height = img.height * 0.5
        img.anchor = position
        self.spreadsheet.add_image(img)

    def overwrite(self, value,row_index,column_index):
        cell = self.spreadsheet.cell(row_index,column_index)
        cell.value = value

    def duplicate_sheet(self,cell_block):
        row_size = len(cell_block)
        column_size = len(cell_block[0])

        for i in range(row_size):
            for j in range(column_size):
                cell = cell_block[i][j]
                new_cell = self.spreadsheet.cell(row=cell.row, column=cell.column,
                                         value=cell.value)
                if cell.has_style:
                    new_cell.font = copy(cell.font)
                    new_cell.border = copy(cell.border)
                    new_cell.fill = copy(cell.fill)
                    new_cell.number_format = copy(cell.number_format)
                    new_cell.protection = copy(cell.protection)
                    new_cell.alignment = copy(cell.alignment)

    def get_block(self,cell_block):
        return self.spreadsheet[cell_block]

    def write_block_to_jury_file(self,cell_block):
        row = 7
        column = 3
        for i in range(len(cell_block)):
            for j in range(len(cell_block[0])):
                self.spreadsheet.cell(row=row+i,column=column+j).value = cell_block[i][j].value

    def write_block_for_jury_comment(self, cell_block_worksheet, row, address):
        cell_block = cell_block_worksheet[address]
        column = 1
        for i in range(len(cell_block)):
            for j in range(len(cell_block[0])):
                try:
                    new_cell = self.spreadsheet.cell(row=row+i,column=column+j)
                    new_cell.value = cell_block_worksheet.cell(row=i+1,column=j+1).value
                except AttributeError:
                    print(f"Probably MergedCell at cell:{i}:{j}")

                if cell_block[i][j].has_style:
                    new_cell.font = copy(cell_block[i][j].font)
                    new_cell.border = copy(cell_block[i][j].border)
                    new_cell.fill = copy(cell_block[i][j].fill)
                    new_cell.number_format = copy(cell_block[i][j].number_format)
                    new_cell.protection = copy(cell_block[i][j].protection)
                    new_cell.alignment = copy(cell_block[i][j].alignment)

    def add_textbox(self,row_index,col_index):
        ttxt = "Write your comment here"
        img = Image("../back_com.png")
        width = img.width * 0.5
        height = img.height * 0.5
        xlwropts = {'width': width, 'height': height}
        self.spreadsheet.insert_textbox(row_index, col_index, ttxt, xlwropts)
