# INFO : Putting Jury's name
# I don't think we need this.

from pysheet_client import PysheetClient
import os
import pickle

if __name__=="__main__":
    instruments = ["Violin","Vocal"]

    with open('jury_score_worksheet2.pickle', 'rb') as f:
        jury_score_worksheet = pickle.load(f)

    for instrument in instruments:
        cdir = os.chdir(instrument)
        file_list = os.listdir()
        file_list.sort()
        print(file_list)
        for file in file_list:
            print(file)
            if file == ".DS_Store":
                continue
            #if file == "Fun_Piano_BEGINNER_AA.xlsx" or file == "Fun_Piano_BEGINNER_DVP.xlsx"or file == "Fun_Piano_ELEMENTARY_AA.xlsx" or file == "Fun_Piano_ELEMENTARY_DVP.xlsx" or file == ".DS_Store":
            #    continue
            pysheet = PysheetClient(file)
            pysheet.access_spreadsheet("PLAYLIST")
            cell_block = pysheet.get_block("B7:E66")

            if "AA" in file:
                jury_name = "Achira Assawadecharit M.Mus"
                category_name = file.replace("AA.xlsx","")
                category_name = category_name.replace("_"," ")
            elif "EK" in file:
                jury_name = "Dr. Eunsoo Kwak"
                category_name = file.replace("EK.xlsx", "")
                category_name = category_name.replace("_", " ")
            elif "YR" in file:
                jury_name = "Youjung Ryu M.Mus"
                category_name = file.replace("YR.xlsx", "")
                category_name = category_name.replace("_", " ")
            elif "DVP" in file:
                jury_name = "Dessy V Pranowo M.Mus"
                category_name = file.replace("DVP.xlsx", "")
                category_name = category_name.replace("_", " ")
            elif "IK" in file:
                jury_name = "Ikuko Kitakado MA"
                category_name = file.replace("IK.xlsx", "")
                category_name = category_name.replace("_", " ")
            elif "PS" in file:
                jury_name = "Pepita Salim B.Mus"
                category_name = file.replace("PS.xlsx", "")
                category_name = category_name.replace("_", " ")

            pysheet.access_spreadsheet("JURI 1")
            row_index = 1
            for row in cell_block:
                if row[1].value == "":
                    break
                number = row[0].value
                full_name = row[1].value
                repertoire = row[3].value

                jury_score_worksheet.cell(row=1, column=3).value = jury_name
                jury_score_worksheet.cell(row=2, column=3).value = category_name
                jury_score_worksheet.cell(row=3, column=3).value = number
                jury_score_worksheet.cell(row=4, column=3).value = full_name
                jury_score_worksheet.cell(row=5, column=3).value = repertoire
                jury_score_worksheet.cell(row =26, column=6).value = jury_name
                print(jury_name,category_name,number,full_name,repertoire)

                address = "A1:K29"
                pysheet.write_block_for_jury_comment(jury_score_worksheet,row_index,address)
                row_index += 29
                pysheet.save()
            print("")

        cdir = os.chdir("../")