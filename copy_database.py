# INFO: This script prepares a list of play each category to excel file
# We might still use this, but a better architecture would be applying the database
# to a server and then read this from the server.

from pysheet_client import PysheetClient
import pandas as pd
import os

cell_block_dict = {
    "Database_Fun Piano BEGINNER.xlsx": "A2:E17",
    "Database_Fun Piano ELEMENTARY.xlsx": "A2:E32",
    "Database_Fun Piano JUNIOR.xlsx": "A2:E9",
    "Database_Regular Piano ELEMENTARY.xlsx": "A2:E36",
    "Database_Regular Piano JUNIOR A.xlsx": "A2:E42",
    "Database_Regular Piano JUNIOR B.xlsx": "A2:E10",
    "Database_Regular Piano LITTLE BEGINNER.xlsx": "A2:E28",
    "Database_Regular Piano PREPARATORY.xlsx": "A2:E45",
    "Database_Regular Piano SENIOR.xlsx": "A2:E15",
    "Database_Violin BEGINNER.xlsx": "A2:E24",
    "Database_Violin ELEMENTARY.xlsx": "A2:E32",
    "Database_Violin JUNIOR.xlsx": "A2:E25",
    "Database_Violin SENIOR.xlsx": "A2:E26",
    "Database_Vocal BEGINNER.xlsx": "A2:E26",
    "Database_Vocal ELEMENTARY.xlsx": "A2:E9",
    "Database_Vocal JUNIOR.xlsx": "A2:E7",
    "Database_Young Artist Piano BAROQUE.xlsx": "A2:E3",
    "Database_Young Artist Piano ETUDE.xlsx": "A2:E3",
    "Database_Young Artist Piano ROMANTIC-MODERN.xlsx": "A2:E19",
    "Database_Young Artist Piano SONATA.xlsx": "A2:E6",
}


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]

def get_cell_block(file_name):
    return cell_block_dict[file_name]

def get_violin_jury_list(file_name,piano_jury_list):
    if "BEGINNER" in file_name:
        jury_list = list(filter(lambda file_name: "BEGINNER" in file_name, violin_jury_list))
    elif "ELEMENTARY" in file_name:
        jury_list = list(filter(lambda file_name: "ELEMENTARY" in file_name, violin_jury_list))
    elif "JUNIOR" in file_name:
        jury_list = list(filter(lambda file_name: "JUNIOR" in file_name, violin_jury_list))
    elif "SENIOR" in file_name:
        jury_list = list(filter(lambda file_name: "JUNIOR" in file_name, violin_jury_list))

    return jury_list

def get_piano_jury_list(file_name,piano_jury_list):
    if "BEGINNER" in file_name and "Fun" in file_name:
        jury_list = list(filter(lambda file_name: "BEGINNER" in file_name and "Fun" in file_name, piano_jury_list))
    elif "ELEMENTARY" in file_name and "Fun" in file_name:
        jury_list = list(filter(lambda file_name: "ELEMENTARY" in file_name and "Fun" in file_name, piano_jury_list))
    elif "JUNIOR" in file_name and "Fun" in file_name:
        jury_list = list(filter(lambda file_name: "JUNIOR" in file_name and "Fun" in file_name, piano_jury_list))
    elif "ELEMENTARY" in file_name and "Regular" in file_name:
        jury_list = list(
            filter(lambda file_name: "ELEMENTARY" in file_name and "Regular" in file_name, piano_jury_list))
    elif "JUNIOR A" in file_name and "Regular" in file_name:
        jury_list = list(filter(lambda file_name: "JUNIOR_A" in file_name and "Regular" in file_name, piano_jury_list))
    elif "JUNIOR B" in file_name and "Regular" in file_name:
        jury_list = list(filter(lambda file_name: "JUNIOR_B" in file_name and "Regular" in file_name, piano_jury_list))
    elif "LITTLE BEGINNER" in file_name and "Regular" in file_name:
        jury_list = list(
            filter(lambda file_name: "LITTLE_BEGINNER" in file_name and "Regular" in file_name, piano_jury_list))
    elif "PREPARATORY" in file_name and "Regular" in file_name:
        jury_list = list(
            filter(lambda file_name: "PREPARATORY" in file_name and "Regular" in file_name, piano_jury_list))
    elif "SENIOR" in file_name and "Regular" in file_name:
        jury_list = list(filter(lambda file_name: "SENIOR" in file_name and "Regular" in file_name, piano_jury_list))
    elif "BAROQUE" in file_name:
        jury_list = list(filter(lambda file_name: "BAROQUE" in file_name, piano_jury_list))
    elif "ETUDE" in file_name:
        jury_list = list(filter(lambda file_name: "ETUDE" in file_name, piano_jury_list))
    elif "ROMANTIC-MODERN" in file_name:
        jury_list = list(filter(lambda file_name: "ROMANTIC-MODERN" in file_name, piano_jury_list))
    elif "SONATA" in file_name:
        jury_list = list(filter(lambda file_name: "SONATA" in file_name, piano_jury_list))

    return jury_list

def get_vocal_jury_list(file_name, vocal_jury_list):
    if "BEGINNER" in file_name:
        jury_list = list(filter(lambda file_name: "BEGINNER" in file_name, vocal_jury_list))
    elif "ELEMENTARY" in file_name:
        jury_list = list(filter(lambda file_name: "ELEMENTARY" in file_name, vocal_jury_list))
    elif "JUNIOR" in file_name:
        jury_list = list(filter(lambda file_name: "JUNIOR" in file_name, vocal_jury_list))
    return jury_list

if __name__ == "__main__":
    cdir = os.chdir("LIST")
    database_list = os.listdir()
    database_list.sort()
    # print(database_list)

    cdir = os.chdir("../Piano")
    piano_jury_list = os.listdir()
    piano_jury_list.sort()
    # print(piano_jury_list)

    cdir = os.chdir("../Violin")
    violin_jury_list = os.listdir()
    violin_jury_list.sort()
    # print(violin_jury_list)

    cdir = os.chdir("../Vocal")
    vocal_jury_list = os.listdir()
    vocal_jury_list.sort()
    # print(vocal_jury_list)

    cdir = os.chdir("../")

    piano_database = list(filter(lambda database_name:  "Piano" in database_name, database_list))
    violin_database = list(filter(lambda database_name:  "Violin" in database_name, database_list))
    vocal_database = list(filter(lambda database_name: "Vocal" in database_name, database_list))

    column = ["tanggal", "email", "nama", "repetoar", "link"]
    # print(piano_database)
    # print(violin_database)
    # print(vocal_database)

    for i in range(len(piano_database)):
        file_name = piano_database[i]
        database_file_path = f"LIST/{file_name}"
        cell_block_address = get_cell_block(file_name)
        spreadsheet = file_name.replace("Database_","")
        spreadsheet = spreadsheet.replace(".xlsx","")

        reader = PysheetClient(database_file_path)
        reader.access_spreadsheet(spreadsheet)
        value = reader.get_block(cell_block_address)
        df = pd.DataFrame(value, columns=column)
        cell_block = df[["nama", "link", "repetoar"]].values.tolist()

        jury_list = get_piano_jury_list(file_name, piano_jury_list)

        for jury_file in jury_list:
            print(f"{file_name} ->{jury_file}")
            writer = PysheetClient(f"Piano/{jury_file}")
            writer.access_spreadsheet("PLAYLIST")
            writer.write_block_to_jury_file(cell_block)
            writer.save()


    for i in range(len(violin_database)):
        file_name = piano_database[i]
        database_file_path = f"LIST/{file_name}"
        cell_block_address = get_cell_block(file_name)
        spreadsheet = file_name.replace("Database_", "")
        spreadsheet = spreadsheet.replace(".xlsx", "")

        reader = PysheetClient(database_file_path)
        reader.access_spreadsheet(spreadsheet)
        value = reader.get_block(cell_block_address)
        df = pd.DataFrame(value, columns=column)
        cell_block = df[["nama", "link", "repetoar"]].values.tolist()

        jury_list = get_violin_jury_list(file_name, violin_jury_list)

        for jury_file in jury_list:
            print(f"{file_name} -> {jury_file}")
            writer = PysheetClient(f"Violin/{jury_file}")
            writer.access_spreadsheet("PLAYLIST")
            writer.write_block_to_jury_file(cell_block)
            writer.save()

    for i in range(len(vocal_database)):
        file_name = piano_database[i]
        database_file_path = f"LIST/{file_name}"
        cell_block_address = get_cell_block(file_name)
        spreadsheet = file_name.replace("Database_", "")
        spreadsheet = spreadsheet.replace(".xlsx", "")

        reader = PysheetClient(database_file_path)
        reader.access_spreadsheet(spreadsheet)
        value = reader.get_block(cell_block_address)
        df = pd.DataFrame(value, columns=column)
        cell_block = df[["nama", "link", "repetoar"]].values.tolist()

        jury_list = get_vocal_jury_list(file_name, vocal_jury_list)

        for jury_file in jury_list:
            print(f"{file_name} -> {jury_file}")
            writer = PysheetClient(f"Vocal/{jury_file}")
            writer.access_spreadsheet("PLAYLIST")
            writer.write_block_to_jury_file(cell_block)
            writer.save()