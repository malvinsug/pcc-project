from pysheet_client import PysheetClient
import pickle

if __name__=="__main__":
    reader = PysheetClient("CATEGORY_SUBCATEGORY_MASTER.xlsx")
    reader.access_spreadsheet("JURI 1")
    values = reader.get_block("A1:K29")
    worksheet = reader.spreadsheet

    with open("jury_score_worksheet2.pickle", "wb") as f:
        pickle.dump(worksheet, f)

    # with open("jury_score_format.pickle", "wb") as f:
    #     pickle.dump(values, f)
